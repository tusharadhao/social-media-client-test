# frozen_string_literal: true

source 'https://rubygems.org'

# Use ActiveRecord ORM for the project
gem 'activerecord'

# Execute rake task related to migration
gem 'standalone_migrations'

# Use postgress database
gem 'pg'

# Use Roda for routing tree web toolkit
gem 'roda'

# Implementation of the JSON specification
gem 'json'

# Use Json Web Token (JWT) for token based authentication
gem 'jwt'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# For test cases implementation
gem 'rspec'

# HTTP REST API client for testing application APIs 
gem 'client-api'

# Byebug is a simple to use and feature rich debugger for Ruby
gem 'byebug'

# factory_bot is a fixtures for testing with rspec
gem 'factory_bot'

# for generate multiple record through seed file
gem 'faker'

# for schedule the task of generate excel sheet
gem 'whenever', require: false

# for generate excel sheet
gem 'rubyzip'
gem 'axlsx'
gem 'axlsx_rails'