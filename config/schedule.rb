require 'axlsx'
require_relative '../app/main'

every 1.minute do
  p = Axlsx::Package.new
  p.workbook.add_worksheet(:name => "Feedback") do |sheet|
    sheet.add_row [
      "Id",
      "comment",
      "User",
        "Post",
      "Owner"
    ]
    Feedback.all.each do |feedback|
      next if feedback.id.nil?
      sheet.add_row [
        feedback.id,
        feedback.comment,
        feedback.user_id,
        feedback.post_id,
        feedback.owner_id
      ]
    end
  end
  p.use_shared_strings = true
  file = File.new("./Feedback_xls/#{Time.now.to_s}.xlsx", "w")
  p.serialize file

end